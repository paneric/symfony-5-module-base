<?php

declare(strict_types=1);

namespace Paneric\SymfonyBaseModule\BaseModuleApc;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BaseModuleApcController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('base_module/index.html.twig', [
            'controller_name' => 'BaseModuleController',
        ]);
    }
}
